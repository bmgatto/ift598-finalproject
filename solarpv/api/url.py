from django.urls import path
from . import views
app_name = 'solarpv'
urlpatterns = [ 
    path('Certificate/', views.CertificateListView.as_view(), name='Certificate_list'), 
    path(r'^Certificate/<pk>', views.CertificateDetailView.as_view(), name='Certificate_detail'), 
]
