from rest_framework import serializers
from solarpv.models import Certificate


class CertificateSerializer(serializers.ModelSerializer): 
    class Meta: 
        model = Certificate
        fields = ['certID', 'certnumber', 'locationID', 'reportnumber', 'contactID', 'teststandard', 'product', 'certissuedate']
