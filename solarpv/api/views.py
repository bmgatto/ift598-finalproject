from rest_framework import generics
from solarpv.models import Certificate
from .serializers import CertificateSerializer
from solarpv.models import Client
from django.db.models import Q

class CertificateListView(generics.ListAPIView): 
    queryset = Certificate.objects.all()
    serializer_class = CertificateSerializer

class CertificateDetailView(generics.RetrieveAPIView): 
    queryset = Certificate.objects.all() 
    serializer_class = CertificateSerializer
