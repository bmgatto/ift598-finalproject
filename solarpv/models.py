from django.db import models

# Create your models here.

class Client(models.Model):
    clientID = models.CharField(max_length=20, primary_key=True)
    clientName = models.CharField(max_length=50)
    clientType = models.CharField(max_length=20)
    def __str__(self):
        return "%s (%s)" % (self.clientName, self.clientID)

class User(models.Model):
    username = models.CharField(max_length=20)
    userID = models.CharField(max_length=20, primary_key=True)
    clientID = models.ForeignKey('Client', on_delete=models.CASCADE)
    firstname = models.CharField(max_length=20)
    middlename = models.CharField(max_length=20, blank=True, null=True)
    lastname = models.CharField(max_length=20)
    jobtitle = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    officephone = models.CharField(max_length=20, blank=True, null=True)
    cellphone = models.CharField(max_length=20, blank=True, null=True)
    prefix = models.CharField(max_length=4, default='Mrs', choices=[('Mrs', 'Mrs.'), ('Ms', 'Ms.'), ('Miss', 'Miss.'), ('Mx', 'Mx.'), ('Misc', 'Misc.'), ('Ind', 'Ind.'), ('Mat', 'M@.'), ('San', 'San.'), ('Und', 'Und.'), ('Na', 'N/A'), ])

class Location(models.Model):
    locationID = models.CharField(max_length=20, primary_key=True)
    clientID = models.ForeignKey('User', on_delete=models.CASCADE)
    address1 = models.CharField(max_length=50)
    address2 = models.CharField(max_length=50)
    city = models.CharField(max_length=20)
    state = models.CharField(max_length=20)
    postalcode = models.CharField(max_length=20)
    country = models.CharField(max_length=20)
    phonenumber = models.CharField(max_length=20)
    faxnumber = models.CharField(max_length=20)

class TestStandard(models.Model):
    standardID = models.CharField(max_length=20, primary_key=True)
    standardname = models.CharField(max_length=50)
    description = models.TextField()
    publisheddate = models.CharField(max_length=20)

class Service(models.Model):
    serviceID = models.CharField(max_length=20, primary_key=True)
    serviceName = models.CharField(max_length=50)
    description = models.TextField()
    FIrequired = models.BooleanField()
    FIfrequency = models.CharField(max_length=20)
    teststandardID = models.ForeignKey('TestStandard', on_delete=models.CASCADE)

class Product(models.Model):
    modelNum = models.CharField(max_length=20, primary_key=True)
    name = models.CharField(max_length=50)
    celltechnology = models.CharField(max_length=20)
    cellmanufacturer = models.CharField(max_length=20)
    numberofcells = models.IntegerField()
    numberofcellsinseries = models.IntegerField()
    numberofseriesstrings = models.IntegerField()
    numberofdiodes = models.IntegerField()
    productlength = models.CharField(max_length=20)
    productwidth = models.CharField(max_length=20)
    productweigtht = models.CharField(max_length=20)
    superstratetype = models.CharField(max_length=20)
    Superstratemanufacturer = models.CharField(max_length=20)
    substratetype = models.CharField(max_length=20)
    Substratemanufacturer = models.CharField(max_length=20)
    frametype = models.CharField(max_length=20)
    Frameadhesive = models.CharField(max_length=20)
    encapsulanttype = models.CharField(max_length=20)
    encapsulantmanufacturer = models.CharField(max_length=20)
    junctionboxtype = models.CharField(max_length=20)
    Junctionboxmanufacturer = models.CharField(max_length=20)

class TestSequence(models.Model):
    sequenceID = models.CharField(max_length=20, primary_key=True)
    SequenceName = models.CharField(max_length=50)

class PerformanceData(models.Model):
    modelNum = models.ForeignKey('Product', on_delete=models.CASCADE)
    sequenceID = models.ForeignKey('TestSequence', on_delete=models.CASCADE)
    maxsystemvoltage = models.FloatField()
    opencircuitvoltagevoc = models.FloatField()
    shortcircuitcurrentisc = models.FloatField()
    voltageatmaxpowervmp = models.FloatField()
    currentatmaxpowerimp = models.FloatField()
    Maxpoweroutputpmp = models.FloatField()
    fillfactorff = models.FloatField()

class Certificate(models.Model):
    certID = models.CharField(max_length=20)
    certnumber = models.CharField(max_length=20, primary_key=True)
    locationID = models.ForeignKey('Location', on_delete=models.CASCADE)
    reportnumber = models.CharField(max_length=20)
    contactID = models.ForeignKey('User', on_delete=models.CASCADE)
    teststandard = models.ForeignKey('TestStandard', on_delete=models.CASCADE)
    product = models.ForeignKey('Product', on_delete=models.CASCADE)
    certissuedate = models.CharField(max_length=20)

