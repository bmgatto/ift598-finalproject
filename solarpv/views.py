from django.shortcuts import render

from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from .forms import joinusForm, searchForm
from solarpv.models import Client
from django.http import HttpResponse
from django.db.models import Q

def signup(request):
    if request.method == 'POST':
        form = joinusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('solarpv/index.html')
    else:
        form = joinusForm()
    return render(request, 'solarpv/joinus.html', {'form': form})

def login(request):
    if request.method == 'POST':
        form = loginForm(request.POST)
        if form.is_valid():
            username = userObj['username']
            if (username == 'admin'):
                return redirect('/admin')
            else:
                return redirect('solarpv/index.html')
    else:
        form = loginForm()
    return render(request, 'solarpv/login.html', {'form': form})
   
def certsearch(request):
    if request.method == 'GET':
        form = searchForm(request.GET)
        if form.is_valid():
            username = form.cleaned_data['search']
            results = Client.objects.filter(Q(clientID__icontains='PMC'))
    else:
        form = searchForm()
    return render(request, 'solarpv/certsearch.html', {'form': form})
   


# Create your views here.
def index(request):
    return render(request, 'solarpv/index.html')

def aboutme(request):
    return render(request, 'solarpv/aboutme.html')

def login(request):
    return render(request, 'solarpv/login.html')

#def joinus(request):
#    return render(request, 'solarpv/joinus.html')


