from django.contrib import admin
from solarpv.models import *

# Register your models here.
#admin.site.register(User)
#admin.site.register(Client)
#admin.site.register(Location)
#admin.site.register(Product)
#admin.site.register(TestSequence)
#admin.site.register(Certificate)





@admin.register(Client)
class CourseAdmin(admin.ModelAdmin): 
    list_display = ['clientID', 'clientName', 'clientType'] 
    fields =  ['clientID', 'clientName', 'clientType'] 
    list_filter = ['clientID', 'clientName'] 
    search_fields = ['clientID', 'clientName'] 
 
@admin.register(Location)
class CourseAdmin(admin.ModelAdmin): 
    list_display = ['locationID', 'address1', 'address2', 'city', 'state', 'postalcode', 'country', 'phonenumber', 'faxnumber']
    fields= ['locationID', 'address1', 'address2', 'city', 'state', 'postalcode', 'country', 'phonenumber', 'faxnumber']
    list_filter = ['locationID', 'address1', 'address2', 'city', 'state', 'postalcode', 'country', 'phonenumber', 'faxnumber']
    search_fields = ['locationID', 'address1', 'address2', 'city', 'state', 'postalcode', 'country', 'phonenumber', 'faxnumber']
 
@admin.register(Product)
class CourseAdmin(admin.ModelAdmin): 
    list_display = ['modelNum', 'name'] 
    fields = ['modelNum', 'name'] 
    list_filter = ['modelNum', 'name'] 
    search_fields = ['modelNum', 'name'] 
 
@admin.register(TestSequence)
class CourseAdmin(admin.ModelAdmin): 
    list_display = ['sequenceID', 'SequenceName'] 
    fields =  ['sequenceID', 'SequenceName'] 
    list_filter = ['sequenceID', 'SequenceName'] 
    search_fields = ['sequenceID', 'SequenceName'] 
 
@admin.register(Certificate)
class CourseAdmin(admin.ModelAdmin): 
    list_display = ['certID', 'certnumber', 'certissuedate'] 
    fields =  ['certID', 'certnumber', 'certissuedate'] 
    list_filter =  ['certID', 'certnumber', 'certissuedate'] 
    search_fields =  ['certID', 'certnumber', 'certissuedate'] 
