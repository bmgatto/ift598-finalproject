from django.forms import ModelForm
from django import forms
from solarpv.models import User

class joinusForm(ModelForm):
    class Meta:
        model = User
        fields = ('userID', 'clientID', 'firstname', 'middlename', 'lastname', 'jobtitle', 'email', 'officephone', 'cellphone', 'prefix',)
#    userID = forms.CharField(label='Username', max_length=100)
#    clientID = forms.CharField(label='Company', max_length=100)
#    firstname = forms.CharField(label='First Name', max_length=100)
#    middlename = forms.CharField(label='Middle Name', max_length=100, required=False)
#    lastname = forms.CharField(label='Last Name', max_length=100)
#    jobtitle = forms.CharField(label='Job Title', max_length=100, required=False)
#    email = forms.CharField(label='Email Address', max_length=100, required=False)
#    officephone = forms.CharField(label='Office Phone', max_length=100, required=False)
#    cellphone = forms.CharField(label='Cell Phone', max_length=100, required=False)
#    prefix = forms.ChoiceField(choices=[('Mrs', 'Mrs.'), ('Ms', 'Ms.'), ('Miss', 'Miss.'), ('Mx', 'Mx.'), ('Misc', 'Misc.'), ('Ind', 'Ind.'), ('Mat', 'M@.'), ('San', 'San.'), ('Und', 'Und.'), ('Na', 'N/A'), ])

class loginForm(forms.Form):
    username = forms.CharField(label='Username', max_length=100)
    password = forms.CharField(label='Password', max_length=100)

class searchForm(forms.Form):
    search = forms.CharField(label='search', max_length=100)
